cd ..
docker-compose stop datanode1
docker-compose stop datanode2
docker-compose stop datanode3

docker-compose build datanode1
docker-compose build datanode2
docker-compose build datanode3

docker-compose up -d datanode1
docker-compose up -d datanode2
docker-compose up -d datanode3
