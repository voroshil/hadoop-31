#!/bin/bash

echo "datanode1" > $HADOOP_PREFIX/etc/hadoop/workers
echo "datanode2" >> $HADOOP_PREFIX/etc/hadoop/workers
echo "datanode3" >> $HADOOP_PREFIX/etc/hadoop/workers

$HADOOP_PREFIX/bin/yarn --config $HADOOP_CONF_DIR --daemon stop historyserver 
$HADOOP_PREFIX/bin/yarn --config $HADOOP_CONF_DIR historyserver
