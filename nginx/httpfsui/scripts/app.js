
$('#removeModal').on('show.bs.modal', removeModalShowListener);
$('#removeModal').find('.btn-primary').on('click', removeModalButtonClickListener);

$("#uploadFileModal").on('show.bs.modal', uploadFileModalShowListener);
$("#uploadFileModal").find('.btn-primary').on('click', uploadFileModalButtonClickListener);

$("#createFolderModal").on('show.bs.modal', createFolderModalShowListener);
$("#createFolderModal").find('.btn-primary').on('click', createFolderButtonClickListener);

$(window).bind('hashchange', function () { //detect hash change
  getListingClick($('<span data-folder="'+window.location.hash.slice(1)+'"></span>'));
});

if (window.location.hash){
  getListingClick($('<span data-folder="'+window.location.hash.slice(1)+'"></span>'));
}else{
  getListingClick();
}

function showAlert(message){
  $(".modal").hide();
  $("#alertMessageModal").find('.modal-body').text(message);
  $("#alertMessageModal").modal('show');
}
function createFolderModalShowListener(event){
  var button = $(event.relatedTarget)
  var folder = button.data('folder')
  $(this).find('.btn-primary').data('folder', folder);
}
function uploadFileModalShowListener(event){
  var button = $(event.relatedTarget)
  var folder = button.data('folder')
  $(this).find('.btn-primary').data('folder', folder);
}
function removeModalShowListener(event){
  var button = $(event.relatedTarget)
  var folder = button.data('folder')
  var filename = button.data('file')

  var modal = $(this);
  modal.find('.modal-body span').text(folder+filename);
  modal.find('.btn-primary').data('folder', folder);
  modal.find('.btn-primary').data('file', filename);
}

function createFolderButtonClickListener(event){
  var button = $(this);
  var folder = button.data('folder')
  var directoryname = $('#directoryName').val();
  $('#createFolderModal').modal('hide');
  createDir(folder+directoryname, function(){
    getListingClick(button);
  });
}

function uploadFileModalButtonClickListener(event){
  var button = $(this);
  var folder = button.data('folder')

  var filename = $('#uploadfile').val();
  var file = $('#uploadfile').prop('files')[0];  

  $('#uploadFileModal').modal('hide');

  var parts = filename.split("\\");
  filename = parts[parts.length-1];
  uploadFile(folder, filename, file, function(){
    getListingClick(button);
  });
}
function removeModalButtonClickListener(event){
  var button = $(this);
  var folder = button.data('folder')
  var filename = button.data('file')
  $('#removeModal').modal('hide');
  remove(folder+filename, function(){
    getListingClick(button);
  });
}

function getListingClick(source){
  var path = "/";
  if (source !== undefined ){
    path = $(source).data("folder") || "/";
  }
  if (path!=="/" && path[path.length-1] !== "/"){

  }
  getListing(path, function(dirs, files){
    var parent;
    var current;
    window.location.hash = "#" + path;
    if (path !== "/"){
      var parts = path.split("/");
      
      lastFolder = parts.pop();
      while(lastFolder === ""){
        lastFolder = parts.pop();
      }
      parent = parts.join("/");
      if (parent === ""){
        parent = "/";
      }
  
      if (path.endsWith("/")){
        current = path;         
      }else{
        current = path + "/";         
      }
    }else{
      current = "/";
    }

    var strTemplate = $("#directoryTemplate").html();
    Mustache.parse(strTemplate);
    var rendered = Mustache.render(strTemplate, {
        parent: parent,
        current: current,
        dirs: dirs,
        files:files
    });
    $("#response").html(rendered);




//    $("#response-raw").text(JSON.stringify(files));
//    $("#response").html(html);
    
  });
}
function uploadFile(current, filename, file, callback){
//  $("#uploadfile").fileupload()
  var form_data = new FormData();                  
  form_data.append('file', file);
  $.ajax({
    url:"/webhdfs/v1"+current+filename+"?op=CREATE&overwrite=true&user.name=root", 
    method:"PUT", 
    contentType: "application/octet-stream",
    processData: false,
    data:file, 
    success: function(data){
      if (typeof(callback) === "function"){
        callback(data);
      }
    },
    error: function(response, data2){
      if (response.status === 200 || response.status === 201){
        if (typeof(callback) === "function"){
          callback(response.statusText);
        }
      }else{
        console.log("error");
        console.log(data);
        console.log(data2);
      }
    }
  });

}
function remove(path, callback){
  $.ajax({
    url:"/webhdfs/v1" + path + "?user.name=root&op=DELETE",
    method: "DELETE",
    success: function (res) {
      if (typeof(callback) === "function"){
        callback(res);
      }
    },
    error: function(response, text){
      if (response.responseJSON !== undefined && response.responseJSON.RemoteException !== undefined){
        showAlert(response.responseJSON.RemoteException.message);
      }else{
        console.log("remove error");
        console.log(response);
        console.log(text);
        showAlert(text);
      }
    }
  });
}
function createDir(path, callback){
  $.ajax({
    url:"/webhdfs/v1" + path + "?user.name=root&op=MKDIRS",
    method: "PUT",
    success: function (res) {
      if (typeof(callback) === "function"){
        callback(res);
      }
    }
  });
}
function getListing(path, callback){
  $.get(
    "/webhdfs/v1" + path, 
    {"user.name": "root", "op":"LISTSTATUS"},
    function (res) {
      var dirs = [];
      var files = [];
      var i;
      for(i=0; i<res.FileStatuses.FileStatus.length; i++){
        var status = res.FileStatuses.FileStatus[i];
        var date = new Date(status.modificationTime);

        var day = ("" + date.getDay()).padStart(2, "0");
        var month = ("" + date.getMonth()).padStart(2, "0");
        var year = ("" + date.getFullYear()).padStart(2, "0");
        var hours = ("" + date.getHours()).padStart(2, "0");
        var minutes = ("" + date.getMinutes()).padStart(2, "0");
        var seconds = ("" + date.getSeconds()).padStart(2, "0");
        var formattedTime = day + "." + month + "."+ year + " " + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
        if (status.type === "FILE"){
          files.push({name:status.pathSuffix,size:status.length,modificationTime:status.modificationTime,modificationTimeStr:formattedTime});
        }else if (status.type="DIRECTORY"){
          dirs.push({name:status.pathSuffix,modificationTime:status.modificationTime,modificationTimeStr:formattedTime});
        }
      }
      callback(dirs, files);
    }
  );
}
